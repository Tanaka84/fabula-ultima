export const FABULA = {};

// Define constants here, such as:
FABULA.foobar = {
  'bas': 'FABULA.bas',
  'bar': 'FABULA.bar'
};

/**
 * The set of Ability Scores used within the sytem.
 * @type {Object}
 */
 FABULA.abilities = {
  "str": "FABULA.AbilityStr",
  "dex": "FABULA.AbilityDex",
  "con": "FABULA.AbilityCon",
  "int": "FABULA.AbilityInt",
  "wis": "FABULA.AbilityWis",
  "cha": "FABULA.AbilityCha"
};

FABULA.abilityAbbreviations = {
  "str": "FABULA.AbilityStrAbbr",
  "dex": "FABULA.AbilityDexAbbr",
  "con": "FABULA.AbilityConAbbr",
  "int": "FABULA.AbilityIntAbbr",
  "wis": "FABULA.AbilityWisAbbr",
  "cha": "FABULA.AbilityChaAbbr"
};